package conexionHibernate;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="cliente")	//mapeo en una tabla llamada clientes
public class Cliente {

	//se crean los dos constructores
	
	public Cliente(String nombre, String apellido, String direccion) {	
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
	}

	public Cliente() {
	}


	//se crean los getters y setters
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellidos) {
		this.apellido = apellidos;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	
	public DetallesCliente getDetallesCliente() {
		return detallesCliente;
	}

	public void setDetallesCliente(DetallesCliente detallesCliente) {
		this.detallesCliente = detallesCliente;
	}
	
	public List<Pedido> getPedidos() {
		return pedidos;
	}

	
	
	//toString
	@Override
	public String toString() {
		return "Cliente [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", direccion=" + direccion + "]";
	}

	
	
	
	
	//se crean las variables de las columnas

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)//vid50 asignar clave principal
	@Column(name="id")
	private int id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="apellido")
	private String apellido;
	
	@Column(name="direccion")
	private String direccion;
	
	@OneToOne(cascade=CascadeType.ALL)	//se crea la relacion con la tabla/clase DetallesCliente
	@JoinColumn(name="id")//esepecifica a traves de q campo esta relacionadas las tablas
	private DetallesCliente detallesCliente;

	//campo que me permite guardar varios pedido
	
	@OneToMany(fetch=FetchType.LAZY/*vid64 especifico el fetch*/, mappedBy="cliente", cascade={CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})//el campo de la tabla pedido
	private List<Pedido> pedidos;
	
	
	//agrego pedidos
	public void agregarPedidos(Pedido elPedido) {
		if(pedidos==null) pedidos=new ArrayList<>();
		
		pedidos.add(elPedido);
		
		elPedido.setCliente(this);
	}

	
}
