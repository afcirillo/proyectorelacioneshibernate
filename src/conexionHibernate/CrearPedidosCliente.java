package conexionHibernate;

import java.util.Date;
import java.util.GregorianCalendar;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CrearPedidosCliente {

public static void main(String[] args) {
		
		SessionFactory miFactory=new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Cliente.class)
				.addAnnotatedClass(DetallesCliente.class)
				.addAnnotatedClass(Pedido.class)
				.buildSessionFactory();
		
		Session miSession=miFactory.openSession();
		
	try {
		
		miSession.beginTransaction();
		//obtener el clientee de la tabla Cliente de la BBDD
		Cliente elCliente=miSession.get(Cliente.class, 5);
		
		//crear Pedidos del Cliente
		Pedido pedido1=new Pedido(new GregorianCalendar(2021,2,9));
		Pedido pedido2=new Pedido(new GregorianCalendar(2021,3,21));
		Pedido pedido3=new Pedido(new GregorianCalendar(2021,4,27));
		
		//agregar Pedidos creados al CLiente creado
		elCliente.agregarPedidos(pedido1);
		elCliente.agregarPedidos(pedido2);
		elCliente.agregarPedidos(pedido3);

		//guardar los Pedidos en la tabla Pedido
		miSession.save(pedido1);
		miSession.save(pedido2);
		miSession.save(pedido3);

		miSession.getTransaction().commit();
			
		System.out.println("Pedidos insertados correctamente en BBDD");
		
			
	}catch(Exception e) {
	
		e.printStackTrace();
		
	}finally {		
		miSession.close();
		miFactory.close();
	}

	}

}
