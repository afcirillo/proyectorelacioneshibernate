package conexionHibernate;

import javax.persistence.*;

@Entity
@Table(name="detalles_cliente")	//mapeo en una tabla llamada detalles clientes
public class DetallesCliente {

	//se crean los dos constructores
	
	public DetallesCliente() {
	}
	

	public DetallesCliente(String web, String tfno, String comentarios) {
		this.web = web;
		this.tfno = tfno;
		this.comentarios = comentarios;
	}

	//se crean los getters y setters
	public void setId(int id) {
		this.id = id;
	}

	public String getWeb() {
		return web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public String getTfno() {
		return tfno;
	}

	public void setTfno(String tfno) {
		this.tfno = tfno;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public Cliente getElCliente() {
		return elCliente;
	}

	public void setElCliente(Cliente elCliente) {
		this.elCliente = elCliente;
	}

	@Override
	public String toString() {
		return "DetallesCliente [id=" + id + ", web=" + web + ", tfno=" + tfno + ", comentarios=" + comentarios + "]";
	}

	

	//se crean las variables de las columnas

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)//vid50 asignar clave principal
	@Column(name="id")
	private int id;
	
	public int getId() {
		return id;
	}

	@Column(name="web")
	private String web;
	
	@Column(name="tfno")
	private String tfno;
	
	@Column(name="comentarios")
	private String comentarios;
	
	//@OneToOne(mappedBy="detallesCliente")//PARA BORRAR DETALLES CLIENTES SIN BORRAR CLIENTE, PASO 1...
	@OneToOne(mappedBy="detallesCliente",cascade=CascadeType.ALL)//se crea la relacion con la tabla/clase Cliente
	private Cliente elCliente;
	

}
