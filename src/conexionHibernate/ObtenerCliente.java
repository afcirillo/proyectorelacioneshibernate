package conexionHibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ObtenerCliente {

	public static void main(String[] args) {

		SessionFactory miFactory=new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Cliente.class)
				.addAnnotatedClass(DetallesCliente.class)
				.addAnnotatedClass(Pedido.class)
				.buildSessionFactory();
		
		Session miSession=miFactory.openSession();
		
		try {
			
			miSession.beginTransaction();
			
			//Obtener DetallesCliente
			
			DetallesCliente DetallesDeCliente=miSession.get(DetallesCliente.class, 1);
			
			System.out.println(DetallesDeCliente);
			
			System.out.println(DetallesDeCliente.getElCliente()); //aqui me dara EL  cliente, con getElCliente
			
			System.out.println("Ahora eliminaremos en cascada");

			miSession.delete(DetallesDeCliente); 
			
			miSession.getTransaction().commit();
			
			
		}catch(Exception ex1) {
				ex1.printStackTrace();//con esto ya no tenemos leak de memoria
			
		}finally {
			miSession.close();

			miFactory.close();
		}

	}

}

