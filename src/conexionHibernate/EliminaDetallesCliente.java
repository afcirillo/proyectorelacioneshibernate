package conexionHibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class EliminaDetallesCliente {

	public static void main(String[] args) {
		
		SessionFactory miFactory=new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Cliente.class)
				.addAnnotatedClass(DetallesCliente.class)
				.addAnnotatedClass(Pedido.class)
				.buildSessionFactory();
		
		Session miSession=miFactory.openSession();
		
		try {
			miSession.beginTransaction();
			
			DetallesCliente detalleDelCliente=miSession.get(DetallesCliente.class, 4);//si no encuentra el cliente se guarda null
			
			detalleDelCliente.getElCliente().setDetallesCliente(null);//PAS0 2 ..CON ESTO QUITLO LA ASOCIACION PARA 
																	//QUE ME QUITE UNO DE LOS TANTOS ERRORES AL QUERER BORRAR.
																	//ADEMAS SE NECESITA BORRAR LA FOREIGN KEY PERO ES PREFERIBLE NO HACERLO
			if(detalleDelCliente!=null) {
				miSession.delete(detalleDelCliente);
			}
			
			miSession.getTransaction().commit();
			
			if(detalleDelCliente!=null) System.out.println("Regsitro eliminado correctamente en BBDD.");
			else System.out.println("No se encontr� el cliente.");
			
			miSession.close();
			
			
		}finally {
			miFactory.close();
		}

	}

}
