package conexionHibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class EliminaCliente {

	public static void main(String[] args) {
		
		SessionFactory miFactory=new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Cliente.class)
				.addAnnotatedClass(DetallesCliente.class)
				.addAnnotatedClass(Pedido.class)
				.buildSessionFactory();
		
		Session miSession=miFactory.openSession();
		
		try {
			miSession.beginTransaction();
			
			Cliente elCliente=miSession.get(Cliente.class, 3);//si no encuentra el cliente se guarda null
			
			if(elCliente!=null) {
				System.out.println("Se eliminará al cliente: "+ elCliente.getNombre());
				miSession.delete(elCliente);
			}
			
			miSession.getTransaction().commit();
			
			if(elCliente!=null) System.out.println("Regsitro eliminado correctamente en BBDD.");
			else System.out.println("No se encontró el cliente.");
			
			miSession.close();
			
			
		}finally {
			miFactory.close();
		}

	}

}
