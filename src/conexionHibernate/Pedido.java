package conexionHibernate;


import java.util.GregorianCalendar;

import javax.persistence.*;


@Entity
@Table(name="pedido")	//mapeo en una tabla llamada pedido
public class Pedido {
	
	//constructores
	
	public Pedido() {
		
	}
	
	public Pedido(GregorianCalendar fecha) {
		this.fecha = fecha;
	}
	
	
	//getters y setters
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	
	
	//toString
	@Override
	public String toString() {
		return "Pedido [id=" + id + ", fecha=" + fecha + ", formaPago=" + formaPago + "]";
	}


	//variables
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Column(name="FECHA")
	private GregorianCalendar fecha;
		
	@Column(name="FORMA_PAGO")
	private String formaPago;
		
	@ManyToOne(cascade={CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})//necesitamos todos menos remove
	@JoinColumn(name="CLIENTE_ID")//esepecifica a traves de q campo esta relacionadas las tablas	
	private Cliente cliente;

		

	
}
