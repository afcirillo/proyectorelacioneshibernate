package pruebasHibernate;

import java.sql.Connection;
import java.sql.DriverManager;

public class PruebasJDBC {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		String jdbcUrl="jdbc:mysql://localhost:3306/relacionesHibernate?useSSL=false";
		
		String usuario="root";
		
		String contra="";	
		
		
		try {
			
			System.out.println("Intentanto conectar con la BBDD: " + jdbcUrl);
			
			Connection miConexion=DriverManager.getConnection(jdbcUrl, usuario, contra);
			
			System.out.println("Conexion exitosa!!");
			
		}catch(Exception e) {
			
			e.printStackTrace();
		}

	}

}
